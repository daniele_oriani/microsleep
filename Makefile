CXXFLAGS := -std=c++11 -Wall -pipe
SOURCE := microsleep.cc
TARGET := microsleep

all: $(TARGET);\

$(TARGET): $(SOURCE);\
  $(LINK.cc) -o $@ $<
