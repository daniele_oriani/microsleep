#include <iostream>
#include <thread>
#include <chrono>

int main(int argc, char** argv)
{
  if(argc != 2)
  {
    std::cerr << "Usage: microsleep [MICROSECONDS]" << std::endl;
    return 1;
  }
  
  unsigned int microsecs = 1;
  microsecs = std::atoi(argv[1]);
  std::this_thread::sleep_for(std::chrono::milliseconds(microsecs));
  return 0;
}
